FROM node:22.2.0-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY src/ ./src
CMD ["node","src/app.js"]
