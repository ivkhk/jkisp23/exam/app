import dotenv from "dotenv";
import { validatePort, validateIP, validateVariableSet } from "../utils/validation.js";

dotenv.config();

const port = process.env.MANAGEMENT_PORT;
validatePort(port,"MANAGEMENT_PORT");

const psql = {};
psql.port = process.env.POSTGRES_PORT;
validatePort(psql.port,"POSTGRES_PORT");
psql.host = process.env.POSTGRES_HOST;
validateIP(psql.host, "POSTGRES_HOST");
psql.user = process.env.POSTGRES_USER;
validateVariableSet(psql.user, "POSTGRES_USER");
psql.password = process.env.POSTGRES_PASSWORD;
validateVariableSet(psql.password, "POSTGRES_PASSWORD");
psql.database = process.env.POSTGRES_DATABASE;
validateVariableSet(psql.database, "POSTGRES_DATABASE");

const rabbitmq = {};
rabbitmq.ip = process.env.RABBITMQ_IP;
validateVariableSet(rabbitmq.ip, "RABBIMQ_IP");

export { port, psql, rabbitmq };
