import { port } from "./config/index.js";
import express from "express";
import cors from "cors";
import router from "./routes/index.js";
import handleConsume from "./utils/consumer.js";

const app = express();

app.use(cors());
app.use(express.json());
app.disable("x-powered-by");

app.use("/api/users", router);

handleConsume();

app.listen(port, () => {
  console.log(`Management app started at port ${port}`);
});
