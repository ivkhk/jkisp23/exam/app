import { DataTypes } from "sequelize"
import { sequelize } from  "../db/db.js"

export const User = sequelize.define('user', {
  username: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  firstname: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  lastname: {
    type: DataTypes.STRING,
    allowNull: false,
  }
},{
  timestamps: false,
});
