import { Sequelize } from "sequelize";
import { psql } from "../config/index.js";

export const sequelize = new Sequelize(psql.database, psql.user, psql.password,{
  host: psql.host,
  dialect: 'postgres'
});
