import amqp from "amqplib";
import { addUser } from "../controllers/users.js";
import { rabbitmq } from "../config/index.js";

const handleConsume = async() => {
  let connected = false;

  while (!connected) {
    try {
      const queue = "management_queue";

      const conn = await amqp.connect(`amqp://${rabbitmq.ip}`);
      const ch = await conn.createChannel();

      console.log(`Connected to RabbtMQ: amqp://${rabbitmq.ip}`);

      await ch.assertQueue(queue, { durable: true });

      ch.consume(queue, (message) => {
        addUser(message.content.toString());
      },{ noAck: true });

      connected = true;

    }catch(error) {
      console.error(`${error} (RabbitMQ)`);
      await new Promise(resolve => setTimeout(resolve, 1000));
    }
  }
}

export default handleConsume;
