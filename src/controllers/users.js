import { User } from "../models/user.js";

export const getUsers = async (req, res) => {
  try {
    const users = await User.findAll({
      attributes: ['id', 'username', 'firstname', 'lastname']
    });
    return res.json(users);
  } catch(error) {
    console.error(`${error}`);
    return res.status(500).json("Internal Server Error occured!");
  }
}

export const addUser = async (user) => {

  let  { username, firstname, lastname} = JSON.parse(user);
  try {

    if (!username?.trim() || !firstname?.trim() || !lastname?.trim()) {
      console.log("Missing necessary fields!");
      return;
    }

    await User.create({
      username: username,
      firstname: firstname,
      lastname: lastname,
    })
  } catch(error) {
    console.error(`${error}`);
  }
}
